package com.metaweb.util.javascript;

import org.mozilla.javascript.Context;
import org.mozilla.javascript.Function;
import org.mozilla.javascript.Scriptable;
import org.mozilla.javascript.ScriptableObject;

public class JSObject extends ScriptableObject {

    private static final long serialVersionUID = 3218033841430800117L;

    protected Scriptable _scope;

    public Scriptable makeJSInstance() {
        Object jsobj = _scope.get(getClassName(), _scope);
        Object[] args = { _scope, this };
        return ((Function)jsobj).construct(Context.getCurrentContext(), _scope, args);
    }

    public Scriptable makeJSInstance(Scriptable scope) {
        _scope = scope;
        Object jsobj = scope.get(getClassName(), scope);
        Object[] args = { scope, this };
        return ((Function)jsobj).construct(Context.getCurrentContext(), scope, args);
    }

    public Scriptable getScope() {
        return _scope;
    }

    public String getClassName() {
        return "JSObject";
    }
}
